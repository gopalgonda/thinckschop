<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'includes/headerlinks.php'
    ?>
</head>

<body>

<?php
include 'includes/header.php'
?>
<!-- End Header -->

<main id="main" data-aos="fade-up">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>Services</h2>
                <ol>
                    <li><a href="index.php">Home</a></li>
                    <li>Services</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Services</h2>
                <h3>OUR WORK/<span>TRAININGS</span>/SERVICES</h3>

            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                    <div class="icon-box-texts">
                        <!--                          <div class="icon"><i class="bx bxl-dribbble"></i></div>-->
                        <!--                          <h4><a href="">Personal Development</a></h4>-->
                        <p>
                            We specialize in providing positive psychology coaching services in many areas where you
                            would like to see yourself better than before, perform better efficiently in your everyday
                            day-to-day tasks, change old non productive habits that are preventing you, change
                            behaviour/thinking, build confidence/kindness/resilience. Navigate and manage your thoughts
                            / mindset, Incorporate agile/creative/design thinking in your personal, and work space to
                            deliver quality results.
                            Everyone who needs to be empowered and needs to be adaptable to changing environments /
                            situations.

                        </p>
                        <p>
                            The core principles of coaching are to know your goal or agenda, awareness or reality of the
                            situation, options to achieve your goal or agenda, actions and accountability towards
                            achieving your goal.

                        </p>
                        <p>
                            Check it out what our clients are saying Link to <a href="#">testimonials</a>
                        </p>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in"
                     data-aos-delay="200">
                    <div class="icon-box-texts">
                        <!--                          <div class="icon"><i class="bx bx-file"></i></div>-->
                        <!--                        <h4><a href="">Transactional</a></h4>-->
                        <p>
                            Self Growth & Improvement Coaching
                            Self-Care / Compassion / Awareness Coaching
                            New Ways of thinking Coaching

                        </p>
                        <h4>Self Growth & Improvement Coaching:</h4>
                        <p>
                            Looking for ways to become better than before OR bounce back better, improve transitional /
                            communication, performance, transform to better self, cultivate good behaviors

                        </p>
                        <p>
                            If your answer is yes, let’s connect and talk about the areas you would like to get better
                            at.
                            Want to connect to share, Schedule an Appointment for 30 mins free consultation call <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank">
                                 click here
                            </a>

                        </p>
                        <p>
                            Connect with via <a href="contact.php">Contact us</a>
                        </p>
                        <h4>Self-Care / Compassion / Awareness Coaching:</h4>
                        <p>
                            Looking for ways to care for yourself, build resilience, awareness, mindfulness,
                            empowerment, and well-being. In addition, adaptability to mindset change.

                        </p>
                        <p>
                            If your answer is yes, let’s connect and talk about the areas you would like to get better
                            at.
                            Want to connect to share, Schedule an Appointment for 30 mins free consultation call <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank">
                                 click here
                            </a>
                        </p>
                        <p>
                            Connect with via <a href="contact.php">Contact us</a>
                        </p>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in"
                     data-aos-delay="300">
                    <div class="icon-box-texts">
                        <!--                        <div class="icon"><i class="bx bx-tachometer"></i></div>-->
                        <!--                        <h4><a href="">Performance</a></h4>-->
                        <h4>
                            New Ways of Thinking Coaching:
                        </h4>
                        <p>
                            Looking for ways to think differently with changing times… Be more agile, creative, add
                            design to thinking...

                        </p>
                        <p>
                            If your answer is yes, let’s connect and talk about the areas you would like to get better
                            at.
                            Want to connect to share, Schedule an Appointment for 30 mins free consultation call <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank">
                                click here
                            </a>

                        </p>
                        <p>
                            Connect with via <a href="contact.php">Contact us</a>
                        </p>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in"
                     data-aos-delay="300">
                    <div class="icon-box-texts">
                        <div class="section-title">

                            <h3>GROUP COACHING<span> / TRAINING</span></h3>

                        </div>
                        <p>
                            Group Training for Kids / Adults <br/>
                            Group coaching for Parents<br/>
                            Group coaching for New Moms<br/>
                        </p>
                        <p>
                            Group Training will be offered in the areas of <b> Agile / Creative / Design Thinking</b>
                        </p>
                        <p>
                            Group Coaching for Parents will be offered in the areas of <b>self-management / stress
                                management / Anxiety management / performance management</b>
                        </p>
                        <p>
                            Group Coaching for new Moms will be offered in the areas of <b>self-management / stress
                                management / Anxiety management / performance management</b>
                        </p>
                        <p>
                            For more information let’s connect and talk about the areas you would like to get better at.
                            Want to connect to share, Schedule an Appointment for 30 mins free consultation call < <
                            Calendly link to be added > >

                        </p>
                        <p>
                            Connect with via <a href="contact.php">Contact us</a>
                        </p>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in"
                     data-aos-delay="300">
                    <div class="icon-box-texts">
                        <div class="section-title">

                            <h3>TRAINING KIDS AGES<span> 7 and ABOVE</span></h3>

                        </div>
                        <p>
                            Between 5 & 9 Between 10 & 12 Between 13 & 15 Between 16 & 19 20+

                        </p>
                        <p>
                            Training to build confidence & trust, overcoming fear / shyness / nervousness will be
                            offered for all ages grouped by ages <br/>
                            5 & 9<br/>
                            10&12<br/>
                            13&15<br/>
                            16&19<br/>
                            20+

                        </p>
                        <p>
                            Training to build mindfulness will be offered for all ages grouped by ages<br/>
                            5 & 9<br/>
                            10&12<br/>
                            13&15<br/>
                            16&19<br/>
                            20+

                        </p>
                        <p>
                            Training to build leadership and public speaking skills will be offered for all ages grouped
                            by ages <br/>
                            5 & 9<br/>
                            10&12<br/>
                            13&15<br/>
                            16&19<br/>
                            20+

                        </p>
                        <p>
                            For more information let’s connect and talk about the areas you would like to get trained
                            and build and develop skills.
                            Want to connect to share, Schedule an Appointment for 30 mins free consultation call <
                            < Calendly link to be added > >
                        </p>
                        <p>
                            Connect with  via <a href="contact.php">Contact us</a>
                        </p>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- End Services Section -->

</main>

<!-- End #main -->

<!-- ======= Footer ======= -->
<?php
include 'includes/footer.php'
?>
<!-- End Footer -->

<?php
include 'includes/footer_scripts.php'
?>

</body>

</html>