<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'includes/headerlinks.php'
    ?>
</head>

<body>

<?php
include 'includes/header.php'
?>
<!-- End Header -->

<main id="main" data-aos="fade-up">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>Explore</h2>
                <ol>
                    <li><a href="index.php">Home</a></li>
                    <li>Explore</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Explore</h2>
                <h3> Explore & Checkout <span>  What is Brewing</span></h3>

            </div>

            <div class="row">
                <div class="col-lg-6" data-aos="fade-right" data-aos-delay="100">
                    <img src="assets/img/about.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center" data-aos="fade-up"
                     data-aos-delay="100">
                    <!--            <h3>Voluptatem dignissimos provident quasi corporis voluptates sit assumenda.</h3>-->

                    <p>
                        Quote of the day | Life Tips | Flow of Thoughts | Equation of the Month | Be the Person you want
                        to be | Find Purpose | Inspiring Stories | Find Meaning | How To...Of the Day

                    </p>
                    <h4>QUOTE OF THE DAY::</h4>
                    <p class="">
                        We are what we repeatedly do, excellence, therefore it is not an act but a habit.
                        -- By Aristotle

                    </p>
                    <h4>
                        What thoughts/reflections/emotions come to mind as you read Quote of the Day?
                    </h4>
                    <p>
                        Want to connect to share, Schedule an Appointment for 30 mins free consultation call <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank">
                             click here
                        </a>
                    </p>
                    <p>
                        Connect with via <a href="contact.php">Contact us</a>
                    </p>

                    <h4>LIFE TIP Of THE DAY::</h4>
                    <p>
                        Withholding feedback is choosing comfort over growth. Staying silent deprives people of the
                        opportunity to learn.
                        If you're worried about hurting their feelings, it's a sign that you haven't earned their trust.
                        In healthy relationships, honesty is an expression of care.
                        -- By Adam Grant

                    </p>
                    <h4>What thoughts/reflections/emotions come to mind as you read Life Tip?</h4>
                    <p>
                        Want to connect to share, Schedule an Appointment for 30 mins free consultation call <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank">
                             click here
                        </a>

                    </p>
                    <p>
                        Connect with via <a href="contact.php">Contact us</a>
                    </p>


                </div>

                <div class="col-12 p-3">
                    <h4>
                        FLOW OF THOUGHTS::
                    </h4>
                    <p>
                        <b>Take a Look... A Thought is</b>
                    </p>

                    <p>
                        An idea, A plan, An opinion, A picture etc., that is formed in our mind; something we think of.
                        Thoughts can take various forms based on the state of the person.
                    </p>
                    <p>
                        <b> If the state</b> is sad, gloomy, unhappy, dejected, vexed, indignant, irritated, irate,
                        agitated,
                        unnerved, jealousy, not confident...etc.

                    </p>
                    <p>
                        <b>Thoughts That Come In This State,</b> bring negative thoughts, drains all energy and leads to
                        path
                        of sadness and then to disaster.
                        Almost all painful feelings have their source, in an incorrect way of looking at reality, no
                        awareness, and when we destroy erroneous views the suffering stops.


                    </p>

                    <h6>
                        What thoughts/reflections/emotions come to mind as you read Flow of thoughts?
                    </h6>
                    <p>
                        Want to connect to share, Schedule an Appointment for 30 mins free consultation call <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank">
                             click here
                        </a>
                    </p>

                    <h4>EQUATION OF THE MONTH::</h4>
                    <p>
                        ENVY = (PRIDE + VANITY) / KINDNESS

                    </p>
                    <h4>
                        What thoughts/reflections/emotions come to mind as you read Equation of the month?

                    </h4>
                    <p>
                        Want to connect to share, Schedule an Appointment for 30 mins free consultation call <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank">
                             click here
                        </a>
                    </p>
                    <p>
                        Connect with via <a href="contact.php">Contact us</a>
                    </p>
                    <h4>BE THE PERSON YOU WANT TO BE::</h4>

                    <p>
                        What thoughts/reflections/emotions come to mind as you read <b>Be the person you wan to be?</b>

                    </p>

                    <p>
                        Want to connect to share, Schedule an Appointment for 30 mins free consultation call <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank">
                             click here
                        </a>

                    </p>

                    <p>
                        Connect with via <a href="contact.php">Contact us</a>

                    </p>
                    <h4>FIND YOUR PURPOSE::</h4>
                    <p>
                        Purpose is something that is done or created with all your heart, mind and soul that gives
                        happiness and makes you feel accomplished.
                        Purpose in life can be looked at as an aim, design your life, setting goals, intention behind
                        each goal, the objective that you love to do the most, and that comes naturally within and
                        requires lots of determination to take the first step.
                        Purpose gets tied to your passion, yearning (deep desire), and deeper happiness

                    </p>
                    <p>
                        The question that can be asked is “What one attends to accomplish or attain and Why?”
                        What thoughts/reflections/emotions come to mind as you read <b>Find your pupose</b>
                    </p>
                    <p>
                        Want to connect to share, Schedule an Appointment for 30 mins free consultation call <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank">
                             click here
                        </a>
                    </p>
                    <p>
                        Connect with via <a href="contact.php">Contact us</a>
                    </p>
                    <h4>FIND YOUR MEANING::</h4>
                    <p>
                        Meaning is something that one does work with lots enthusiasm, shows passion, and is in the flow.
                        Basically having a purpose will give you the true meaning of life. Living a meaningful life
                        gives confidence, connection with your inner being, and peace.
                    </p>
                    <p>
                        The question that can be asked is “How do you want to be seen as a person and Why?”
                    </p>
                    <p>
                        What thoughts/reflections/emotions come to mind as you read <b>Find your pupose</b>
                        Want to connect to share, Schedule an Appointment for 30 mins free consultation call <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank">
                             click here
                        </a>

                    </p>
                    <p>
                        Connect with via <a href="contact.php">Contact us</a>
                    </p>
                    <h4>
                        HOW TO… OF THE DAY::
                    </h4>
                    <p>
                        Food for thought <br/>
                        What does your ideal look like?<br/>
                        What are your childhood dreams that you want to fulfill?<br/>
                        What are things you like to admire?<br/>
                        What habits you would like to break<br/>
                        What to do break stress<br/>
                        What to do relax

                    </p>

                    <p>
                        What thoughts/reflections/emotions come to mind as you read <b>How To … Of the Day?</b>
                        Want to connect to share, Schedule an Appointment for 30 mins free consultation call <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank">
                             click here
                        </a>

                    </p>
                    <p>
                        Connect with via <a href="contact.php">Contact us</a>
                    </p>








                </div>

            </div>

        </div>
    </section>
    <!-- End About Section -->


</main>

<!-- End #main -->

<!-- ======= Footer ======= -->
<?php
include 'includes/footer.php'
?>
<!-- End Footer -->

<?php
include 'includes/footer_scripts.php'
?>

</body>

</html>