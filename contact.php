<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'includes/headerlinks.php'
    ?>
</head>

<body>

<?php
include 'includes/header.php'
?>
<!-- End Header -->

<main id="main" data-aos="fade-up">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>Contact</h2>
                <ol>
                    <li><a href="index.htm">Home</a></li>
                    <li>Contact</li>
                </ol>
            </div>

        </div>
    </section>
    <!-- End Breadcrumbs -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Contact</h2>
                <h3><span>Thinking Scape</span></h3>

            </div>

            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-6">
                    <div class="info-box mb-4">
                        <i class="bx bx-map"></i>
                        <h3>Our Address</h3>
                        <p>A108 Adam Street, New York, NY 535022</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="info-box  mb-4">
                        <i class="bx bx-envelope"></i>
                        <h3>Email Us</h3>
                        <p>contact@thinkingscape.com</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="info-box  mb-4">
                        <i class="bx bx-phone-call"></i>
                        <h3>Call Us</h3>
                        <p>732-423-4781</p>
                    </div>
                </div>

            </div>

            <div class="row" data-aos="fade-up" data-aos-delay="100">

                <div class="col-lg-6 ">
                    <!-- <iframe class="mb-4 mb-lg-0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 384px;" allowfullscreen></iframe>-->

                    <div class="info-box  mb-4">
                        <!--                          <i class="bx bx-phone-call"></i>-->
                        <i class='bx bx-time'></i>
                        <h3>Timings</h3>
                        <p class="p-2">
                            Schedule an Appointment for 30 mins free consultation call < < Calendly lMon to Fri from
                        </p>
                        <p>Mon to Fri from 6:30pm to 8:30pm</p>
                        <p>Sat & Sun from 10:00am to 4:00pm</p>
                    </div>

                </div>

                <div class="col-lg-6">
                    <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                        <div class="row">
                            <div class="col form-group">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Name"
                                       required>
                            </div>
                            <div class="col form-group">
                                <input type="number" class="form-control" name="number" id="number"
                                       placeholder="Phone Number" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col form-group">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Email Address"
                                       required>
                            </div>
                            <div class="col form-group">
                                <input type="text" class="form-control" name="whatIsOnYourMaind" id="whatIsOnYourMaind"
                                       placeholder="What is on your mind?"
                                       required>
                            </div>
                        </div>


                        <div class="form-group">
                            <input type="text" class="form-control" name="questionForUs" id="questionForUs" placeholder="Question for us?"
                                   required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="additionalInformation" id="additionalInformation" placeholder="Additional Information?"
                                   required>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="tellUsHowYouHearAboutUs" rows="5" placeholder="Tell us how you hear about us?"
                                      required></textarea>
                        </div>
                        <div class="my-3">
                            <div class="loading">Loading</div>
                            <div class="error-message"></div>
                            <div class="sent-message">Your message has been sent. Thank you!</div>
                        </div>
                        <div class="text-center">
                            <button type="submit">Submit</button>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </section><!-- End Contact Section -->

</main>

<!-- End #main -->

<!-- ======= Footer ======= -->
<?php
include 'includes/footer.php'
?>
<!-- End Footer -->

<?php
include 'includes/footer_scripts.php'
?>

</body>

</html>