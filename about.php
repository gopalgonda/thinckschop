<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'includes/headerlinks.php'
    ?>
</head>

<body>

<?php
include 'includes/header.php'
?>
<!-- End Header -->

<main id="main" data-aos="fade-up">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>About</h2>
                <ol>
                    <li><a href="index.php">Home</a></li>
                    <li>About</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>About</h2>
                <h3>Find Out More <span>About Us</span></h3>
                <p>ThinkingScape... Feel The Best & Be The Best
                </p>

            </div>

            <div class="row">
                <div class="col-lg-6" data-aos="fade-right" data-aos-delay="100">
                    <img src="assets/img/about.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center" data-aos="fade-up"
                     data-aos-delay="100">
                    <!--            <h3>Voluptatem dignissimos provident quasi corporis voluptates sit assumenda.</h3>-->

                    <p>
                        We all know life is like an exam OR Life is a journey, we go through life changing moments after
                        every exam or every experience. Then we reach a point in life telling ourselves either 'what I
                        want to be', or 'now i'm stuck', or 'cannot change' or 'life is very difficult', or 'I'm
                        depressed' etc. etc. etc.

                    </p>
                    <p class="fst-italic">
                        With constant studying of human behaviour, learning state of mind, and observing how external
                        factors can influence the state of mind that defines the behaviour at any given moment has
                        helped to start coaching services providing company. Started the journey with family, friends,
                        and now extending to larger audiences who are looking for

                    </p>

                    <ul style="    list-style: circle;margin-left: 33px;line-height: 2px;">
                        <li>Increasing productivity or improve performance</li>
                        <li>Better self management</li>
                        <li>Transformation to better self</li>
                        <li>Changing habits, thinking, and behaviour</li>
                        <li>Increasing hope, kinReducing stress, depression, and anxiety levels</li>
                        <li>dness, and forgiveness</li>
                        <li>Going after dreams, goals, and purpose</li>

                    </ul>

                    <p>
                        Are you stuck? Come and chat with us, schedule a 30min Free consultation call
                        <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank">
                            click here
                        </a>
                    </p>


                </div>

                <div class="col-12 p-3">
                    <h4>
                        ThinkingScape’s Founders Pathway:

                    </h4>

                    <p>
                        Latha Dandu’s ThinkingScape is a pathway to fulfill the dream of unlocking the potential by
                        understanding the what, maximizing my potential and how I want to be seen in the world, has
                        directed me towards what is needed to achieve my dreams.
                        AND here I’m with a mission -

                    </p>
                    <p>
                        Unlocking my client's potential by understanding their why and what, maximizing their potential
                        by how they want to see themselves in the world, and holding space for them to achieve their
                        goals and dreams using positive psychology & coaching.

                    </p>

                    <h6>
                        My Degrees and Credentials earned along the way --
                    </h6>
                    <ul style="list-style: circle;margin-left: 33px;">
                        <li>B.E in Computer Science from Bombay University, India.</li>
                        <li>MBA in Marketing from Strayer University, USA.</li>
                        <li>Certified in Applied Positive Psychology (CAPP) from Flourishing Center, NY, USA.</li>
                        <li>Certified in Applied Positive Psychology Coaching (APPC) from Flourishing Center, NY, USA.
                        </li>
                        <li>Associate Certified Coach from International Coaching Federation (ICF).</li>
                        <li>Design Thinking Practitioner (DTP) from Verizon Process and Innovation, NJ, USA.</li>
                        <li>Positive Education Certified (PEC) from Positive Education, NY, USA.</li>
                        <li>Phd in Neuroscience Psychology (Marching towards it)</li>

                    </ul>
                    <p>
                        What is your ThinkingScape and Pathway? Connect with us and Let’s chat, schedule 30min Free
                        consultation call <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank" >
                             click here
                        </a>

                    </p>
                   <p>
                       Connect with  via <a href="">Contact Us</a>
                   </p>
                </div>

            </div>

        </div>
    </section>
    <!-- End About Section -->


</main>

<!-- End #main -->

<!-- ======= Footer ======= -->
<?php
include 'includes/footer.php'
?>
<!-- End Footer -->

<?php
include 'includes/footer_scripts.php'
?>

</body>

</html>