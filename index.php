<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'includes/headerlinks.php'
    ?>
</head>

<body>

<?php
include 'includes/header.php'
?>
<!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
        <h1>Welcome to <span>ThinkingScape</span></h1>
        <h2>Feel The Best & Be The Best
        </h2>
        <div class="d-flex">
            <a type="button" class="btn-get-started scrollto" data-bs-toggle="modal" data-bs-target="#bookappointment">Book
                Appointment</a>

        </div>
    </div>
</section><!-- End Hero -->

<main id="main">

    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <!--                <h2>Our Happy Clients</h2>-->
                <h3><span>Highlights</span></h3>
            </div>

            <div class="card m-4">
                <div class="card-body">
                    <p class="text-center">
                        Everyone is busy, has many challenges to handle, and people have very little time to think about
                        themselves OR express themselves OR reflect back on the choices they have made..
                        Currently our audiences are

                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="services-ul">
                        <ul>


                            <li><i class='bx bx-check-double'></i> New and Experienced Parents</li>
                            <li><i class='bx bx-check-double'></i> Adults and Teenagers/adolescents</li>
                            <li><i class='bx bx-check-double'></i> Professionals from any area like IT, business,
                                finance, etc.
                            </li>
                            <li><i class='bx bx-check-double'></i> Parents handling mentally and physically challenged
                                kids
                            </li>
                            <li><i class='bx bx-check-double'></i> Parents handling chronic illness family members</li>


                        </ul>
                        <p>
                            What is on your Mind? Come and Let’s chat with <a href="contact.php">Contact </a>
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div>
                        <img src="assets/img/hilights2.gif" class="img-fluid"/>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Featured Services Section -->


    <section class="services">
        <div class="container">
            <div class="section-title">
                <!--        <h2>About</h2>-->
                <h3>Who we <span>Are & our</span> Purpose</h3>

            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                    <div class="icon-box">
                        <div class="icon"><i class='bx bxs-badge'></i></div>
                        <h4><a href="javascript.valid(0)">Our Purpose</a></h4>
                        <p>
                            We Empathize with our clients, Understand them and Believe that THEY CAN CHANGE THE WORLD.
                        </p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in"
                     data-aos-delay="200">
                    <div class="icon-box">
                        <div class="icon"><i class='bx bxs-disc'></i></div>
                        <h4><a href="">Our Mission</a></h4>
                        <p>
                            Unlocking our client's potential by understanding their why, maximizing their potential by
                            how they want to see themselves in the world, and directing them towards what is needed to
                            achieve their dreams.
                        </p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in"
                     data-aos-delay="300">
                    <div class="icon-box">
                        <div class="icon"><i class='bx bx-unite'></i></div>
                        <h4><a href="">Our Objective</a></h4>
                        <p>Expand our Positive Psychology coaching services to all who need help. Checkout our
                            <a href="services.php">Services</a></p>
                    </div>
                </div>

            </div>

            <div class="text-center p-3">
                <b>
                    And learn more <a href="about.php">About Us</a>
                </b>
            </div>
        </div>
    </section>

    <section class="group-coaching-training">
        <div class="container">
            <div class="section-title">
                <!--        <h2>About</h2>-->
                <h3>Group<span> Coaching </span>Training</h3>

                <p>
                    Agile / creative / design thinking Training Group coaching for Parents Group coaching for New Moms
                </p>
                <p>
                    Create your own groups of 3, 5 or 8 <br>
                    OR<br>
                    Based on the need Groups of 3, 5 or 8 will be formed

                </p>
            </div>
        </div>
    </section>


    <section class="ourworkandtraining services">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <!--        <h2>About</h2>-->
                <h3>Our work <span>Training & </span> Services</h3>

            </div>
        </div>

        <div class="container" data-aos="fade-up">

            <div class="row">
                <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                    <div class="icon-box taining-services">
                        <div class="icon"><i class="bx bxl-dribbble"></i></div>
                        <b><a href="">Self Growth & Improvement Coaching
                            </a></b>
                        <!--                        <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>-->
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in"
                     data-aos-delay="200">
                    <div class="icon-box taining-services">
                        <div class="icon"><i class="bx bx-file"></i></div>
                        <b><a href="">Self-Care / Compassion / Awareness coaching</a></b>
                        <!--                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>-->
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in"
                     data-aos-delay="300">
                    <div class="icon-box taining-services">
                        <div class="icon"><i class="bx bx-tachometer"></i></div>
                        <b>
                            <a href="">New Ways of thinking coaching </a>
                        </b>
                        <!--                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>-->
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                    <div class="icon-box taining-services">
                        <div class="icon"><i class="bx bx-world"></i></div>
                        <b><a href="">Group Coaching / Training</a></b>
                        <!--<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>-->
                    </div>
                </div>
                <!--                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="100">-->
                <!--                    <div class="icon-box">-->
                <!--                        <div class="icon"><i class="bx bx-world"></i></div>-->
                <!--                        <h4><a href="">Behavioral</a></h4>-->
                <!--                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>-->
                <!--                    </div>-->
                <!--                </div>-->

            </div>

        </div>
    </section>


    <!-- ======= Clients Section ======= -->
    <!--    <section id="clients" class="clients section-bg">-->
    <!--        <div class="container" data-aos="zoom-in">-->
    <!--            <div class="section-title">-->
    <!--                <h2>Our Happy Clients</h2>-->
    <!--                <h3>Our Happy<span>Clients</span></h3>-->
    <!--                <p>-->
    <!--                    Hide this section, this section will be similar tp Testimonials-->
    <!--                </p>-->
    <!--            </div>-->
    <!--            <div class="row">-->
    <!---->
    <!--                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">-->
    <!--                    <img src="assets/img/clients/client-1.png" class="img-fluid" alt="">-->
    <!--                </div>-->
    <!---->
    <!--                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">-->
    <!--                    <img src="assets/img/clients/client-2.png" class="img-fluid" alt="">-->
    <!--                </div>-->
    <!---->
    <!--                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">-->
    <!--                    <img src="assets/img/clients/client-3.png" class="img-fluid" alt="">-->
    <!--                </div>-->
    <!---->
    <!--                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">-->
    <!--                    <img src="assets/img/clients/client-4.png" class="img-fluid" alt="">-->
    <!--                </div>-->
    <!---->
    <!--                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">-->
    <!--                    <img src="assets/img/clients/client-5.png" class="img-fluid" alt="">-->
    <!--                </div>-->
    <!---->
    <!--                <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">-->
    <!--                    <img src="assets/img/clients/client-6.png" class="img-fluid" alt="">-->
    <!--                </div>-->
    <!---->
    <!--            </div>-->
    <!---->
    <!--        </div>-->
    <!--    </section>-->
    <!-- End Clients Section -->


    <section class="ourworkandtraining">
        <div class="container" data-aos="zoom-in">
            <div class="section-title">
                <!--        <h2>About</h2>-->
                <h3>Training<span> Kids age 7</span> & Above</h3>
                <p>
                    Between 5 & 9 Between 10 & 12 Between 13 & 15 Between 16 & 19 20+
                    Following Training services will be offered
                </p>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="">
                        <h2>&ensp;</h2>
                    </div>
                    <div>
                        <ul style="list-style: circle;margin-left: 33px; padding: 20px;">
                            <li>Building Trust & Confidence</li>
                            <li>Overcoming Shyness</li>
                            <li>Overcoming nervousness</li>
                            <li>Thoughts, Feeling and Emotions Check</li>
                            <li>Leadership and public speaking</li>
                        </ul>

                    </div>
                </div>

                <div class="col-md-6">

                    <div>
                        <img src="assets/img/kid_pic.jpg" class="img-fluid">
                    </div>

                </div>
            </div>

        </div>
    </section>

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
        <div class="container" data-aos="zoom-in">
            <div class="section-title">
                <h2>Testimonials</h2>
                <p class="text-white">
                    Add Scrolling view, existing one is good
                </p>
            </div>
            <div class="testimonials-slider swiper-container" data-aos="fade-up" data-aos-delay="100">
                <div class="swiper-wrapper">

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
                            <h3>Saul Goodman</h3>
                            <h4>Ceo &amp; Founder</h4>
                            <p>
                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit
                                rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam,
                                risus at semper.
                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt="">
                            <h3>Sara Wilsson</h3>
                            <h4>Designer</h4>
                            <p>
                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid
                                cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet
                                legam anim culpa.
                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt="">
                            <h3>Jena Karlis</h3>
                            <h4>Store Owner</h4>
                            <p>
                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam
                                duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="assets/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt="">
                            <h3>Matt Brandon</h3>
                            <h4>Freelancer</h4>
                            <p>
                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat
                                minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore
                                labore illum veniam.
                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                    <div class="swiper-slide">
                        <div class="testimonial-item">
                            <img src="assets/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt="">
                            <h3>John Larson</h3>
                            <h4>Entrepreneur</h4>
                            <p>
                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster
                                veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam
                                culpa fore nisi cillum quid.
                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                            </p>
                        </div>
                    </div><!-- End testimonial item -->

                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>
    <!-- End Testimonials Section -->

    <section>
        <div class="container" data-aos="zoom-in">
            <div class="section-title">
                <!--        <h2>About</h2>-->
                <h3>What <span> is brewing</span></h3>
                <p>
                    Between 5 & 9 Between 10 & 12 Between 13 & 15 Between 16 & 19 20+
                    Following Training services will be offered
                </p>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="">
                        <div>
                            <img src="assets/img/kid_pic.jpg" class="img-fluid">
                        </div>
                    </div>

                </div>

                <div class="col-md-6">


                    <!--<div class="section-title">-->
                    <!--    <h2>Blocks of 6</h2>-->
                    <!--</div>-->
                    <div>
                        <ul style="list-style: circle;margin-left: 33px; padding: 20px;">
                            <li>Inspiring Stories shared by Clients</li>
                            <li>Quote of the day</li>
                            <li>Surprising Facts - Life Tips</li>
                            <li>Equation of the Month</li>
                            <li>Be the person you want to be...</li>
                            <li>Find your Purpose...</li>
                        </ul>
                        <p>
                            All these blocks link to <a href="explore.php"> Explore</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>F.A.Q</h2>
            </div>

            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <ul class="faq-list">

                        <li>
                            <div data-bs-toggle="collapse" class="collapsed question" href="#faq1">
                                What is Coaching? <i class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq1" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Using Positive Psychology coaching methodologies, allowing clients to maximise their
                                    potential to help overcome their obstacles and show them a pathway of becoming
                                    better than before OR Bounce back better.

                                </p>
                            </div>
                        </li>

                        <li>
                            <div data-bs-toggle="collapse" href="#faq2" class="collapsed question">
                                How is Coaching different from Consulting?
                                <i class="bi bi-chevron-down icon-show"></i><i class="bi bi-chevron-up icon-close"></i>
                            </div>
                            <div id="faq2" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Coaching is bringing awareness about clients potential whereas consulting
                                    professional technical advice.

                                </p>
                            </div>
                        </li>

                        <li>
                            <div data-bs-toggle="collapse" href="#faq3" class="collapsed question">
                                How is Coaching different from Advisory? <i class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq3" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Coaching is bringing awareness about clients potential whereas consulting
                                    professional technical advice.
                                </p>
                            </div>
                        </li>


                    </ul>
                    <p>
                        Have more questions <a href="faq.php">FAQs</a>
                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- End Frequently Asked Questions Section -->

</main>

<!-- End #main -->

<!-- ======= Footer ======= -->
<?php
include 'includes/footer.php'
?>
<!-- End Footer -->

<?php
include 'includes/footer_scripts.php'
?>

</body>

</html>