<footer id="footer">

    <div class="footer-newsletter">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <h4>Join Our Newsletter</h4>
                    <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                    <form action="" method="post">
                        <input type="email" name="email"><input type="submit" value="Subscribe">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-contact">
                    <h3>System Requirementse<span>.</span></h3>
                    <p>
                        Goal is to have scalable platform
                        Similar to the content management system. Ability to manage the static pages and having admin
                        capabilities to modify the content.

                    </p>
                    <p>
                        <br/>
                        <strong>Phone:</strong> 732-423-4781<br>
                        <strong>Email:</strong> contact@thinkingscape.com<br>
                    </p>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Recent Posts</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">NewsLetter</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Contact Us</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Our Services</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Disclaimer</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Terms & Conditions</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy Policy</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Cookie - Policy</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Copyright text</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Our Social Networks</h4>
                    <p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p>
                    <div class="social-links mt-3">
                        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                        <a href="#" class="twitter"><i class="bx bxl-youtube"></i></a>



                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container py-4">
        <div class="copyright">
            &copy; Copyright <strong><span>ThinkingScape</span></strong>. All Rights Reserved
        </div>

    </div>
</footer>








<!-- Modal -->
<div class="modal fade" id="bookappointment" tabindex="-1" aria-labelledby="bookappointmentLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="bookappointmentLabel">Book Appointment</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                       <tbody>
                       <tr>
                           <td><p>6:30pm to 8:30pm</p></td>
                           <td align="center">
                               <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank" >
                                    click here
                               </a>
                           </td>
                       </tr>
                       <tr>
                           <td><p> 10:00am to 4:00pm</p></td>
                           <td align="center">
                               <a href="https://calendly.com/time2think/30-mins-consultation?month=2021-08"  target="_blank" >
                                    click here
                               </a>
                           </td>
                       </tr>
                       </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>