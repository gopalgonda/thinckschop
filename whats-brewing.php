<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'includes/headerlinks.php'
    ?>
</head>

<body>

<?php
include 'includes/header.php'
?>
<!-- End Header -->

<main id="main" data-aos="fade-up">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>What’s Brewing</h2>
                <ol>
                    <li><a href="index.php">Home</a></li>
                    <li>What’s Brewing</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->


    <section>
        <div class="container" data-aos="zoom-in">
            <div class="section-title">
                <!--        <h2>About</h2>-->
                <h3>What <span> is brewing</span></h3>
                <p>
                    Between 5 & 9 Between 10 & 12 Between 13 & 15 Between 16 & 19 20+
                    Following Training services will be offered
                </p>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="">
                        <div>
                            <img src="assets/img/kid_pic.jpg" class="img-fluid">
                        </div>
                    </div>

                </div>

                <div class="col-md-6">


                    <div class="section-title">
                        <h2>Blocks of 6</h2>
                    </div>
                    <div>
                        <ul style="list-style: circle;margin-left: 33px; padding: 20px;">
                            <li>Inspiring Stories shared by Clients</li>
                            <li>Quote of the day</li>
                            <li>Surprising Facts - Life Tips</li>
                            <li>Equation of the Month</li>
                            <li>Be the person you want to be...</li>
                            <li>Find your Purpose...</li>
                        </ul>
                        <p>
                            All these blocks link to <a href="explore.php"> Explore</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>


</main>

<!-- End #main -->

<!-- ======= Footer ======= -->
<?php
include 'includes/footer.php'
?>
<!-- End Footer -->

<?php
include 'includes/footer_scripts.php'
?>

</body>

</html>