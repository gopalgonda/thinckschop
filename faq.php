<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include 'includes/headerlinks.php'
    ?>
</head>

<body>

<?php
include 'includes/header.php'
?>
<!-- End Header -->

<main id="main" data-aos="fade-up">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>FAQ</h2>
                <ol>
                    <li><a href="index.htm">Home</a></li>
                    <li>FAQ</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->


    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>F.A.Q</h2>
                <h3>Frequently Asked <span>Questions</span></h3>

            </div>

            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <ul class="faq-list">

                        <li>
                            <div data-bs-toggle="collapse" class="collapsed question" href="#faq1">
                                What is Coaching? <i class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq1" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Using Positive Psychology coaching methodologies, allowing clients to maximise their
                                    potential to help overcome their obstacles and show them a pathway of becoming
                                    better than before OR Bounce back better.
                                    Bringing insights with their clients' reflections, looking at the future with great
                                    positivity, and transforming to become a better person than they ever imagined.

                                </p>
                            </div>
                        </li>

                        <li>
                            <div data-bs-toggle="collapse" href="#faq2" class="collapsed question">
                                How is Coaching different from Consulting?
                                <i class="bi bi-chevron-down icon-show"></i><i class="bi bi-chevron-up icon-close"></i>
                            </div>
                            <div id="faq2" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Coaching is bringing awareness about clients potential whereas consulting
                                    professional technical advice.
                                </p>
                            </div>
                        </li>

                        <li>
                            <div data-bs-toggle="collapse" href="#faq3" class="collapsed question">
                                How is Coaching different from Advisory? <i class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq3" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Coaching is bringing awareness about clients potential whereas consulting
                                    professional technical advice.
                                </p>
                            </div>
                        </li>

                        <li>
                            <div data-bs-toggle="collapse" href="#faq4" class="collapsed question">
                                How is Coaching different from Counselling? <i class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq4" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Coaching is bringing awareness about clients potential whereas Counselling is
                                    helping deal with past pain and overcoming emotional pains/problems.
                                </p>
                            </div>
                        </li>

                        <li>
                            <div data-bs-toggle="collapse" href="#faq5" class="collapsed question">
                                How is Coaching different from Teaching? <i class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq5" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Coaching is bringing awareness about clients potential whereas Teaching is
                                    developing a skill or knowledge
                                </p>
                            </div>
                        </li>

                        <li>
                            <div data-bs-toggle="collapse" href="#faq6" class="collapsed question">
                                How is Coaching different from Mentoring? <i
                                        class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq6" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Coaching is bringing awareness about clients potential whereas mentoring is telling
                                    how to do and what to do in developing skills often based on past situations.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div data-bs-toggle="collapse" href="#faq7" class="collapsed question">
                                How is Coaching different from Therapy? <i
                                        class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq7" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Coaching is bringing awareness about clients potential whereas Therapy is often
                                    dealing and healing the past pain or finding a resolution to past pain. Therapists
                                    are considered as experts holding answers to the problems clients face.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div data-bs-toggle="collapse" href="#faq8" class="collapsed question">
                                What to expect at the end of our sessions?<i
                                        class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq8" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Every client session is different and all depends on clients' openness to thinking
                                    and believing in themselves that they can make a difference.
                                </p>
                            </div>
                        </li>

                        <li>
                            <div data-bs-toggle="collapse" href="#faq9" class="collapsed question">
                                What if I don’t like the service? OR Is Coaching the right service for me?
                                <i class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq9" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Clients’ openness to broaden themselves matters, if for some reason the client is
                                    closed and always thinking of past, past pains / emotions then the coaching is not
                                    for the client. They should be checking a therapist or a counselor that best fits
                                    clients needs.

                                </p>
                            </div>
                        </li>
                        <li>
                            <div data-bs-toggle="collapse" href="#faq9" class="collapsed question">
                                What if I don’t like the service? OR Is Coaching the right service for me?
                                <i class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq9" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Clients’ openness to broaden themselves matters, if for some reason the client is
                                    closed and always thinking of past, past pains / emotions then the coaching is not
                                    for the client. They should be checking a therapist or a counselor that best fits
                                    clients needs.

                                </p>
                            </div>
                        </li>
                        <li>
                            <div data-bs-toggle="collapse" href="#faq10" class="collapsed question">
                                What if I want advice and solutions to my problems?
                                <i class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq10" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Clients’ openness to broaden themselves matters, if the client is looking for quick
                                    fixes without understanding what client really needs then the coaching is not for
                                    the client. They should be checking a counsellor or a mentor or an advisor that best
                                    fits clients needs.

                                </p>
                            </div>
                        </li>
                        <li>
                            <div data-bs-toggle="collapse" href="#faq11" class="collapsed question">
                                What is the one ask from the client?
                                <i class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq11" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    We provide a space to think and reflect and the client's openness to make a change.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div data-bs-toggle="collapse" href="#faq12" class="collapsed question">
                                How do I know if I am making a change? OR I did not make any change OR has it affected
                                me even more?
                                <i class="bi bi-chevron-down icon-show"></i><i
                                        class="bi bi-chevron-up icon-close"></i></div>
                            <div id="faq12" class="collapse" data-bs-parent=".faq-list">
                                <p>
                                    Client’s openness is very important for any change, and if client is showing
                                    resistance OR is causing more damage the contract will be terminated.
                                </p>
                            </div>
                        </li>


                    </ul>
                </div>
            </div>

        </div>
    </section>
    <!-- End Frequently Asked Questions Section -->

</main>

<!-- End #main -->

<!-- ======= Footer ======= -->
<?php
include 'includes/footer.php'
?>
<!-- End Footer -->

<?php
include 'includes/footer_scripts.php'
?>

</body>

</html>